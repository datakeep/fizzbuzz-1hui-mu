package jp.alhinc.kana;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author kana
 *
 */
public class FizzBuzzTest{

	@BeforeClass
	public static void setUpBeforeClass() throws Exception{
		System.out.println("setUpBeforeClass");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception{
		System.out.println("tearDownAfterClass");
	}

	@Before
	public void setUp() throws Exception{
		System.out.println("setUp");
	}

	@After
	public void tearDown() throws Exception{
		System.out.println("tearDown");
	}

	@Test
	public void checkFizzBuzz_01() {
		System.out.println("checkFizzBuzz_01");
		assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
	}

	@Test
	public void checkFizzBuzz_02() {
		System.out.println("checkFizzBuzz_02");
		assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
	}

	@Test
	public void checkFizzBuzz_03() {
		System.out.println("checkFizzBuzz_03");
		assertEquals("FizzBuzz",FizzBuzz.checkFizzBuzz(45));
	}

	@Test
	public void checkFizzBuzz_04() {
		System.out.println("checkFizzBuzz_04");
		assertEquals("44",FizzBuzz.checkFizzBuzz(44));
	}

	@Test
	public void checkFizzBuzz_05() {
		System.out.println("checkFizzBuzz_05");
		assertEquals("46",FizzBuzz.checkFizzBuzz(46));
	}
}